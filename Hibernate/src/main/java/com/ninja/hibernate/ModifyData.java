/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 *
 * @author lil_ninja88
 */


public class ModifyData {
    protected SessionFactory sessionFactory;
 
    protected void setup() {
    	final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    	try {
    	    sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    	} catch (Exception ex) {
    	    StandardServiceRegistryBuilder.destroy(registry);
    	}
    }
 
    protected void exit() {
    	sessionFactory.close();
    }
 
    protected void create() {
    	Book read = new Book();
    	read.setTitle("Rich Dad Poor Dad");
    	read.setAuthor("Robert T. Kiyosaki");
     
        Session session = sessionFactory.openSession();
        session.beginTransaction();
     
        session.save(read);
     
        session.getTransaction().commit();
        session.close();
    }
 
    protected void read() {
    	Session session = sessionFactory.openSession();
    	 
        long id = 1;
        Book read = session.get(Book.class, id);
     
        System.out.println("Book Title: " + read.getTitle());
        System.out.println("Book Author: " + read.getAuthor());
     
        session.close();
    
    }
 
    protected void update() {
    	Book read = new Book();
        read.setId(4);
        read.setTitle("Where the Sidewalk Ends");
        read.setAuthor("Shel Silverstein");
         
        Session session = sessionFactory.openSession();
        session.beginTransaction();
     
        session.update(read);
     
        session.getTransaction().commit();
        session.close();

    }
 
    protected void delete() {
    	Book read = new Book();
        read.setId(5);
     
        Session session = sessionFactory.openSession();
        session.beginTransaction();
     
        session.delete(read);
     
        session.getTransaction().commit();
        session.close();
    }
 
    public static void main(String[] args) {
        ModifyData data = new ModifyData();
        data.setup();
        data.create();
        data.read();
        data.update();
        data.delete();
        data.exit();
    }
}
