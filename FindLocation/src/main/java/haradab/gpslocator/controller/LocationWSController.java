package haradab.gpslocator.controller;

import haradab.gpslocator.model.Location;
import haradab.gpslocator.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class LocationWSController {

    @Autowired
    private LocationService service;

    @MessageMapping("/locations")
    @SendTo("/topic/locations")
    public List<Location> getAll(String message) {
        return service.getAll();
    }

}
