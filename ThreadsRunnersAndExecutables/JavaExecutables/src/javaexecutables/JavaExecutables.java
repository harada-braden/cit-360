/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaexecutables;

import java.util.concurrent.Executors;

/**
 *
 * @author lil_ninja88
 */
public class JavaExecutables {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Get total available processing cores
        int cores = Runtime.getRuntime().availableProcessors();
        java.util.concurrent.ExecutorService service = Executors.newFixedThreadPool(cores);
        
        // Execute tasks
        for (int i = 0; i < 100; i++) {
            service.execute(new CpuIntensiveTask());
        }
        
    }
    
}

class CpuIntensiveTask implements Runnable {
    @Override
    public void run() {
        
    }
}
